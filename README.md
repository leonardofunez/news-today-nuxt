# news

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate

# Install Node-Sass and Sass-Loader
$ yarn add -D node-sass sass-loader@10.1.1

# Rebuild node-sass
$ npm rebuild node-sass
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
