export const SITE_URL = "https://news-today-nuxt.netlify.app";
export const MAIN_URL = "https://dev-today-news.pantheonsite.io";
export const APP_TITLE = "News Today";
export const COLORS = {
    white:    "#FFFFFF",
    light:    "#F4F4FF",
    black:    "#000000",
    dark:     "#0D2452",
    darker:   "#06183C",
    darkest:  "#010B20",
    red:      "#FF0024",
    fucsia:   "#FF007A",
    fucsia_h: "#E6016E"
};